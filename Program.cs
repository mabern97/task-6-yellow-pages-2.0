﻿using System;
using System.Collections.Generic;

namespace YellowPages2._0
{
    class MainClass
    {
        static List<Person> Persons;

        public static void Main(string[] args)
        {
            string[] Persons = new string[] {
                "John Doe",
                "John Appleseed N/A 84128563",
                "Nikola Tesla N/A 19357269",
                "Bill Gates Microsoft 39572485",
                "Steve Jobs Apple 39419381",
                "Steve Wozniak Apple 29319483",
                "Elon Musk Telsa/SpaceX 29148721",
                "Jack Ma Alibaba",
                "Tim Cook Apple 49548816",
                "Jack Dorsey Twitter",
                "Larry Page Google 39429184",
                "Larry Ellison Oracle",
                "Jeff Bezos Amazon",
                "Michael Dell Dell",
                "Mark Zuckerberg Facebook",
            };

            Setup(Persons);

            bool appState = true;
            while (appState)
            {
                PrintPersonList();
                appState = UserInput();
            }
        }

        public static Person CreatePerson(string FullName)
        {
            return new Person(FullName);
        }

        public static bool UserInput()
        {
            Console.Write("Please enter a name to search: ");
            string input = Console.ReadLine();
            bool state = true;

            switch (input)
            {
                case "exit":
                    state = false;
                    break;
                default:
                    Console.WriteLine($"Got '{input}'");
                    Search(input);
                    break;
            }

            return state;
        }

        /*
         * Sets up the application by creating a new Person list and then
         * adding all names to the Person list.
         */
        public static void Setup(string[] nameList)
        {
            Persons = new List<Person>();

            foreach (string Person in nameList)
            {
                Persons.Add(CreatePerson(Person));
            }
        }
        /*
         * The method that handles searching the Person list and dictating
         * what a match is (partial or full)
         */
        public static void Search(string str)
        {
            List<ContactMatch> matches = new List<ContactMatch>();

            Console.WriteLine($"\nSearching Person list for '{str}': ");
            foreach (Person person in Persons)
            {
                bool matchMade = false;
                ContactMatch match = new ContactMatch(person);

                string firstName = person.FirstName;
                string lastName = person.LastName;
                string fullName = person.FullName;

                // Check if the searched string partially resembles the first name
                if (firstName.ToLower().Contains(str) || firstName.Contains(str) && !matches.Contains(match))
                {
                    matchMade = true;
                    match.DeclarePartial();
                    match.DeclareType(ContactMatch.Type.FirstName);
                }

                // Check if the searched string partially resembles the last name
                else if (lastName.ToLower().Contains(str) || lastName.Contains(str) && !matches.Contains(match))
                {
                    matchMade = true;
                    match.DeclarePartial();
                    match.DeclareType(ContactMatch.Type.LastName);
                }

                // Check if the searched string partially resembles the full name
                else if (fullName.ToLower().Contains(str) || fullName.Contains(str) && !matches.Contains(match))
                {
                    matchMade = true;
                    match.DeclarePartial();
                    match.DeclareType(ContactMatch.Type.FullName);
                }

                // Check if the searched string fully resembles the full name
                else if (fullName.Equals(str) && !matches.Contains(match))
                {
                    matchMade = true;
                    match.DeclareFull();
                    match.DeclareType(ContactMatch.Type.FullName);
                }

                // Check if the searched string fully resembles the first name
                else if (firstName.Equals(str) && !matches.Contains(match))
                {
                    matchMade = true;
                    match.DeclareFull();
                    match.DeclareType(ContactMatch.Type.FirstName);
                }

                // Check if the searched string fully resembles the last name
                else if (lastName.Equals(str) && !matches.Contains(match))
                {
                    matchMade = true;
                    match.DeclareFull();
                    match.DeclareType(ContactMatch.Type.LastName);
                }

                if (matchMade)
                    matches.Add(match);
            }

            SetConsoleColor(ConsoleColor.DarkYellow);
            foreach(ContactMatch match in matches)
            {
                string matchStr = match.MatchLevel == ContactMatch.Level.Full ? "Full" : "Partial";

                Console.WriteLine($"({matchStr} match on {match.MatchType}) {match.Contact.ToString()}");
            }

            SetConsoleColor(null);

            Console.Write("\n Press any key to perform a new search");
            Console.ReadKey();
            Console.Clear();
        }

        /*
         * Prints out a welcome message and all the Persons in the list.
         */
        static void PrintPersonList()
        {
            Console.Clear();
            Console.WriteLine("Welcome to the Yellow Pages!");
            Console.WriteLine("In a moment you will be able to search for your contacts");
            SetConsoleColor(ConsoleColor.Red);
            Console.WriteLine("If you wish to exit at any time, please type 'exit' in the search field.");
            SetConsoleColor(null);

            Console.WriteLine($"Listing all {Persons.Count} Contacts:");

            SetConsoleColor(ConsoleColor.DarkMagenta);
            foreach (Person Person in Persons)
            {
                Console.WriteLine($"{Person}");
            }
            SetConsoleColor(null);
        }

        /*
         * Sets the color of text shown in the console window.
         * If no color is given, then by default the console
         * text color will be set to white.
         */
        static void SetConsoleColor(ConsoleColor? color)
        {
            if (color.HasValue)
            {
                Console.ForegroundColor = color.Value;
                return;
            }

            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}