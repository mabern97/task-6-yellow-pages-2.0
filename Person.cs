﻿using System;
using System.Text;

namespace YellowPages2._0
{
    public class Person
    {
        private Telephone phone; 
        public string FirstName
        {
            get; private set;
        }

        public string LastName
        {
            get; private set;
        }

        public string FullName
        {
            get; private set;
        }

        public string Company
        {
            get; private set;
        }

        public string Phone
        {
            get => phone.Number; private set => new Telephone(value);
        }

        public Person(string fullName)
        {
            if (fullName.Contains(" "))
            {
                string[] name = fullName.Split(' ');
                FirstName = name[0];
                LastName = name[1];
                FullName = $"{name[0]} {name[1]}";

                if (name.Length > 2 && name[2].Length != 0)
                {
                    Company = name[2];
                } else
                {
                    this.Company = "N/A";
                }

                if (name.Length > 3 && name[3].Length != 0)
                {
                    phone = new Telephone(name[3]);
                } else
                {
                    phone = new Telephone("");
                }
            }
            else
            {
                throw new Exception("Invalid full name given.");
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"-- {FirstName} {LastName}\n");
            sb.Append($"    First Name: {FirstName}\n");
            sb.Append($"    Last Name: {LastName}\n");
            sb.Append($"    Company: {Company}\n");
            sb.Append($"    Phone Number: {Phone}\n");
            sb.Append($"--\n");

            //return $"{FirstName} {LastName} {(Company != string.Empty ? $"({Company})" : "")}";
            return sb.ToString();
        }
    }
}
