﻿using System;
namespace YellowPages2._0
{
    public class ContactMatch
    {
        public enum Level
        {
            Full,
            Partial
        }

        public enum Type
        {
            FullName,
            FirstName,
            LastName
        }

        public Person Contact { get; private set; }
        public Level MatchLevel { get; set; }
        public Type MatchType { get; set; }

        public ContactMatch(Person person)
        {
            Contact = person;
        }

        public void DeclareFull()
        {
            MatchLevel = Level.Full;
        }

        public void DeclarePartial()
        {
            MatchLevel = Level.Partial;
        }

        public void DeclareType(Type type)
        {
            MatchType = type;
        }
    }

    public class Telephone
    {
        public string Number
        {
            get; private set;
        }

        public Telephone(string phoneNumber)
        {
            bool parse = true;
            if (phoneNumber.Length == 0)
            {
                Number = "Private Number";
                parse = false;
            }
            else if (phoneNumber.Length < 8)
            {
                Number = "Invalid Number Provided";
                parse = false;
            }

            if (!parse)
                return;

            string groupA = phoneNumber.Substring(0, 3);
            string groupB = phoneNumber.Substring(3, 2);
            string groupC = phoneNumber.Substring(5, 3);

            Number = $"{groupA} {groupB} {groupC}";
        }
    }
}
